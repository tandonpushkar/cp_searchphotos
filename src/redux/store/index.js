
import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers/index';
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['searchHistory']
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

let middleware = [];
middleware = [...middleware, thunk];

const store = createStore(
    persistedReducer,
    compose(applyMiddleware(...middleware))
);
const getStore = () => {
    return store;
}

export { store, getStore };





