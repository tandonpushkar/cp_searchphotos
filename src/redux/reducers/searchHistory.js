let intialState = {
    searchHistory: [],
};
const searchHistory = (state = intialState, action) => {
    switch (action.type) {
        case 'SET_SEARCH_HISTORY':
            let oldArray = state.searchHistory;
            let obj = oldArray.find(o => o.value === action.payload.value);
            if (obj === undefined) {
                oldArray = [action.payload, ...oldArray]
            }
            return {
                ...state,
                searchHistory: oldArray,
            };
        case 'CLEAR_SEARCH_HISTORY':
            return {
                ...state,
                searchHistory: [],
            };
        default:
            return state;
    }
};
export default searchHistory;
