import { combineReducers } from "redux";
import searchHistory from "./searchHistory";

const appReducer = combineReducers({
  searchHistory
});

const rootReducer = (state, action) => {
  return appReducer(state, action)
}

export default rootReducer;
