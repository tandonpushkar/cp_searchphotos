export const setSearchHistory = value => {
    return { type: 'SET_SEARCH_HISTORY', payload: value }
};

export const clearSearchHistory = () => {
    return { type: 'CLEAR_SEARCH_HISTORY' }
};
