import { useEffect, useState } from 'react'
import axios from 'axios'
import { useDispatch } from 'react-redux'
import { setSearchHistory } from '../redux/actions/searchHistory'
import { apiUrlDefault, apiUrlSearch } from '../config'

const usePhotoSearch = (query, pageNumber) => {


    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(false)
    const [photos, setPhotos] = useState([])
    const [hasMore, setHasMore] = useState(false);
    const dispatch = useDispatch();

    useEffect(() => {
        setPhotos([])
    }, [query])

    useEffect(() => {
        setLoading(true)
        setError(false)
        let cancel

        //If the there is no search input then the deafult images api will call else search api will call...
        let reqObject = query.length === 0 ? {
            method: 'GET',
            url: apiUrlDefault,
            params: { api_key: process.env.REACT_APP_API_KEY, safe_search: 3, per_page: 20, page: pageNumber, format: 'json', nojsoncallback: 1 },
            cancelToken: new axios.CancelToken(c => cancel = c)
        } : {
            method: 'GET',
            url: apiUrlSearch,
            params: { api_key: process.env.REACT_APP_API_KEY, text: query, sort: "relevance", per_page: 20, page: pageNumber, format: 'json', nojsoncallback: 1 },
            cancelToken: new axios.CancelToken(c => cancel = c)
        }

        //http request for fetching images....

        axios(reqObject).then(res => {

            //merging new photos with previous photos
            setPhotos(prevPhotos => {
                return [...new Set([...prevPhotos, ...res.data.photos.photo.map((pic) => {
                    let srcPath = 'https://farm' + pic.farm + '.staticflickr.com/' + pic.server + '/' + pic.id + '_' + pic.secret + '.jpg';

                    //creating image object for react-grid-gallery package
                    let imgObject = {
                        src: srcPath,
                        thumbnail: srcPath,
                        thumbnailWidth: 320,
                        thumbnailHeight: 174,
                        caption: pic.title
                    };
                    return imgObject;
                })])]
            })

            //setting redux state for search suggestion
            if (query.length > 0) {

                //creating search suggestion object for autocomplete input
                let searchObject = {
                    value: query,
                };
                dispatch(setSearchHistory(searchObject))
            }


            setHasMore(res.data.photos.photo.length > 0)
            setLoading(false)
        }).catch(e => {
            if (axios.isCancel(e)) return
            setError(true)
        })
        return () => cancel()
    }, [query, pageNumber, dispatch])

    return { loading, error, photos, hasMore }
}

export default usePhotoSearch;
