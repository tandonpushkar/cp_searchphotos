import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import About from '../../screens/About';
import Home from '../../screens/Home';

export const MainStack = () => {
    return (
        <Switch>
            <Route exact path="/">
                <Home />
            </Route>
            <Route path="/about">
                <About />
            </Route>
        </Switch>
    );
}