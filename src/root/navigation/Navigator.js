import { BrowserRouter as Router } from "react-router-dom";
import Navbar from "../../components/Navbar";
import { MainStack } from "./Routes";

const Navigator = () => {
    return (
        <Router>
            <Navbar />
            <MainStack />
        </Router>
    );
}
export default Navigator;
