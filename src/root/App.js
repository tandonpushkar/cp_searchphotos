import { Provider } from 'react-redux';
import { store } from '../redux/store/index';
import { PersistGate } from 'redux-persist/integration/react';
import { persistStore } from 'redux-persist'
import Navigator from './navigation/Navigator';
const persistedStore = persistStore(store)

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistedStore} loading={null}>
        <Navigator />
      </PersistGate>
    </Provider>
  );
}

export default App;
