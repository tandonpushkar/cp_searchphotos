
import React from "react";
import { Divider } from "antd";
import { Typography } from 'antd';
import { styles } from "../styles/theme";
const { Title } = Typography;

const About = () => {

    return (
        <div style={styles.aboutContainer}>

            <Title level={3}>
                About search photos app
            </Title>
            <Title level={4} >
                Simple React Application to search photos from flickr , classplus assignment.
            </Title>
            <Title level={4}>v1.0.0 : Built with ❤️ using React and Antd</Title>
            <Title level={5} style={styles.aboutFeatures}>
                1. Default - By default, show the images from https://www.flickr.com/services/api/flickr.photos.getRecent.html <br /><br />
                2. Search - A Search bar should be placed in the header. When a user is typing, display results from https://www.flickr.com/services/api/flickr.photos.search.html <br /><br />
                3. Infinite Scroll - As you scroll down to the bottom of the page, display more results if there are more results. <br /><br />
                4. Suggestions - Save their search queries(in the browser itself) so that the next time they come back, you can suggest search queries (like as a list/tags near the search bar). <br /><br />
                5. Preview - Clicking on a photo in the results will bring the photo up in a modal. <br /><br />
            </Title>

            <Divider />

            <Title level={4}>
                Pushkar Tandon , MCA
            </Title>
            <Title level={5} style={styles.aboutFeatureContainer}>
                <div>
                    Portfolio :{" "}
                    <a
                        href="https://tandonpushkar.com"
                        rel="noopener noreferrer"
                        target="_blank"
                        style={styles.link}
                    >
                        tandonpushkar.com
                    </a>
                </div>

                <div>
                    Github Profile :{" "}
                    <a
                        href="https://github.com/tandonpushkar"
                        rel="noopener noreferrer"
                        target="_blank"
                        style={styles.link}
                    >
                        tandonpushkar/github
                    </a>

                </div>

                <div>
                    LinkedIn Profile :{" "}
                    <a
                        href="https://in.linkedin.com/in/tandonpushkar"
                        rel="noopener noreferrer"
                        target="_blank"
                        style={styles.link}
                    >
                        tandonpushkar/linkedIn
                    </a>
                </div>
            </Title>
        </div>
    );
}

export default About;