import { Button, Divider, Spin } from 'antd';
import React, { useCallback, useRef, useState } from 'react'
import Gallery from 'react-grid-gallery';
import useDebounce from '../hooks/useDebounce';
import usePhotoSearch from '../hooks/usePhotoSearch';
import { AutoComplete } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { clearSearchHistory } from '../redux/actions/searchHistory';
import { styles } from '../styles/theme';

const Home = () => {
    const observer = useRef();
    const [query, setQuery] = useState('');
    const [pageNumber, setPageNumber] = useState(1);
    const { debouncedValue } = useDebounce(query, 600);  //custom hook for search optimization
    const {
        photos,
        hasMore,
        loading,
        error
    } = usePhotoSearch(debouncedValue, pageNumber);  //custom hook for searching photos and infinity scroll

    const searchHistory = useSelector(state => state.searchHistory.searchHistory); //redux state to persist search history
    const dispatch = useDispatch();


    //function to detect last item in galleryView
    const lastPhotoElementRef = useCallback(node => {
        if (loading) return
        if (observer.current) observer.current.disconnect()
        observer.current = new IntersectionObserver(entries => {
            if (entries[0].isIntersecting && hasMore) {
                setPageNumber(prevPageNumber => prevPageNumber + 1)
            }
        })
        if (node) observer.current.observe(node)
    }, [loading, hasMore])

    //search onchange input function
    const handleSearch = (value) => {
        setQuery(value)
        setPageNumber(1)
    }

    //clear history function
    const clearSearch = () => {
        dispatch(clearSearchHistory());
        handleSearch('');
    }

    return (
        <>
            <div style={styles.homeContainer}>
                <AutoComplete
                    style={styles.autoInput}
                    dropdownRender={menu => (
                        <div>
                            {menu}
                            <Divider style={styles.divider} />
                            <div style={styles.clearButton}>
                                <Button onClick={clearSearch} type="primary" danger>
                                    Clear
                                </Button>
                            </div>
                        </div>
                    )}
                    value={query}
                    onSelect={handleSearch}
                    searchValue={query}
                    onSearch={handleSearch}
                    options={searchHistory}
                    placeholder="Search Photos..."
                    filterOption={(inputValue, option) =>
                        option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1
                    }
                />
            </div>

            <div className="site-layout" style={styles.galleryContainer}>
                <Gallery
                    images={photos}
                    thumbnailImageComponent={({ item }) => {
                        return <div
                            ref={lastPhotoElementRef}
                            key={item.src}
                            style={{ ...styles.thumbnailComponent, background: `url(${item.src})` }} />
                    }} />
                {
                    loading && <div style={styles.galleryLoader}>
                        <Spin size="large" />
                    </div>
                }
                <div>{error && 'Error'}</div>
            </div>
        </>
    )
}


export default Home;
