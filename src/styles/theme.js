export const styles = {
    homeContainer: {
        width: '100%',
        position: 'fixed',
        height: 60,
        marginTop: 64,
        backgroundColor: 'white',
        zIndex: 1,
    },
    autoInput: {
        padding: '0 50px',
        width: '100%',
        marginTop: 15
    },
    clearButton: {
        display: 'flex',
        flexWrap: 'nowrap',
        padding: 8
    },
    galleryContainer: {
        padding: '0 50px',
        marginTop: 134
    },
    thumbnailComponent: {

        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        height: '100%',
        width: '100%'
    },
    galleryLoader: {
        height: 174,
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    divider: { margin: '4px 0' },
    aboutContainer: {
        marginTop: 94,
        textAlign: "center"
    },
    aboutFeatures: {
        margin: 30,
        textAlign: "left"
    },
    link: {
        textDecoration: "none",
        color: "blue"
    },
    aboutFeatureContainer: {
        paddingBottom: 50
    },
    header: {
        height: 64,
        position: 'fixed',
        zIndex: 1,
        width: '100%'
    },
}