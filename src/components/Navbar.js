
import '../styles/header.css'
import React from 'react'
import { Layout, Menu } from 'antd';
import { Link } from 'react-router-dom';
import { styles } from '../styles/theme';

const { Header } = Layout;


const Navbar = () => {
    return (
        <Layout >
            <Header style={styles.header}>
                <div className="logo" />
                <Menu theme="dark" mode="horizontal" >
                    <Menu.Item key="1">  <Link to="/">Search Photos</Link></Menu.Item>
                    <Menu.Item key="2"> <Link to="/about">About</Link></Menu.Item>
                </Menu>
            </Header>
        </Layout>
    )
}

export default Navbar;


